package de.danielprinz.AlienAutoExecutor.Manager;

import java.util.ArrayList;

/**
 * Created by daniela on 12.10.2016.
 */
public class Command {

    String name;
    String helpMessage;
    ArrayList<String> subcommands;
    String[] aliases;

    public Command(String name, String helpMessage, ArrayList<String> subcommands, ArrayList<String> aliases) {
        this.name = name;
        this.helpMessage = helpMessage;
        this.subcommands = subcommands;
        this.aliases = aliases.toArray(new String[aliases.size()]);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHelpMessage() {
        return helpMessage;
    }

    public void setHelpMessage(String helpMessage) {
        this.helpMessage = helpMessage;
    }

    public ArrayList<String> getSubcommands() {
        return subcommands;
    }

    public void setSubcommands(ArrayList<String> subcommands) {
        this.subcommands = subcommands;
    }

    public String[] getAliases() {
        return aliases;
    }

    public void setAliases(String[] aliases) {
        this.aliases = aliases;
    }
}
