package de.danielprinz.AlienAutoExecutor.Commands;

import de.danielprinz.AlienAutoExecutor.Main;
import de.danielprinz.AlienAutoExecutor.Manager.Command;
import de.danielprinz.AlienAutoExecutor.Methods.CommandInitializer;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by daniela on 12.10.2016.
 */
public class AlienAutoExecutorCommandExecutor extends CommandInitializer<Main> {

    public AlienAutoExecutorCommandExecutor(Main plugin, String command, String description, String... aliases) {
        super(plugin, command, description, aliases);
    }

    @Override
    public boolean execute(CommandSender cs, String label, String[] args) {

        if(args.length == 1) {
            if (args[0].equals("reload")) {
                if(!cs.hasPermission("alienautoexecutor.reload")) {
                    cs.sendMessage(this.plugin.prefix + ChatColor.DARK_RED + "You have no permission to execute this command!");
                    return false;
                }
                this.plugin.restartPlugin();
                cs.sendMessage(this.plugin.prefix + ChatColor.GREEN + "The plugin reloaded successfully.");
                return false;
            }
            cs.sendMessage(this.getHelp());
            return false;
        }

        cs.sendMessage(this.getHelp());
        return false;
    }

    private String getHelp() {

        String s = "";

        s += ChatColor.DARK_AQUA + "\n===== Help for AlienAutoExecutor =====\n" +
                ChatColor.GOLD + "/aae reload" + ChatColor.GRAY + " | " + ChatColor.DARK_AQUA + "Reloads the plugin.";

        for(Map.Entry<String, Command> entry : this.plugin.commands.entrySet()) {
            Command cmd = entry.getValue();
            s += ChatColor.GOLD + "\n/" + cmd.getName() + ChatColor.GRAY + " | " + ChatColor.DARK_AQUA + cmd.getHelpMessage();
        }

        return s;

    }

    @Override
    public List<String> tabComplete(CommandSender cs, String label, String[] args) {
        List<String> list = new ArrayList<String>();
        list.add("reload");
        return list;
    }

}
