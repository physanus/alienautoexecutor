package de.danielprinz.AlienAutoExecutor.Commands;

import de.danielprinz.AlienAutoExecutor.Main;
import de.danielprinz.AlienAutoExecutor.Manager.Command;
import de.danielprinz.AlienAutoExecutor.Methods.CommandInitializer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by daniela on 12.10.2016.
 */
public class AutoCommandExecutor extends CommandInitializer<Main> {

    public AutoCommandExecutor(Main plugin, String command, String description, String... aliases) {
        super(plugin, command, description, aliases);
    }

    @Override
    public boolean execute(CommandSender cs, String label, String[] args) {

        Command command = this.plugin.commands.get(this.command);

        ArrayList<String> subcommands = command.getSubcommands();

        int variablesInSubcommands = 0;
        for(int i = 1; i <= 9; i++) {
            Boolean contained = false;
            for(String cmd : subcommands) {
                if(cmd.contains("$" + i)) {
                    contained = true;
                }
            }
            variablesInSubcommands += contained ? 1 : 0;
            contained = false;
        }

        if(!(args.length == variablesInSubcommands)) {
            cs.sendMessage(this.plugin.prefix + ChatColor.DARK_RED + "Arguments not met:");
            cs.sendMessage(ChatColor.GOLD + "/" + command.getName() + ChatColor.GRAY + " | " + ChatColor.DARK_AQUA + command.getHelpMessage());
            return false;
        }

        this.plugin.commandExecutor.load(subcommands, cs, args);
        return false;
    }

    @Override
    public List<String> tabComplete(CommandSender cs, String label, String[] args) {
        List<String> list = new ArrayList<String>();
        for(Player all : Bukkit.getOnlinePlayers()) {
            list.add(all.getDisplayName());
        }
        return list;
    }

}
