package de.danielprinz.AlienAutoExecutor;

import de.danielprinz.AlienAutoExecutor.Commands.AlienAutoExecutorCommandExecutor;
import de.danielprinz.AlienAutoExecutor.Commands.AutoCommandExecutor;
import de.danielprinz.AlienAutoExecutor.Manager.Command;
import de.danielprinz.AlienAutoExecutor.Methods.CommandExecutor;
import de.danielprinz.AlienAutoExecutor.Methods.CommandParser;
import de.danielprinz.AlienAutoExecutor.Methods.CommandUnregisterer;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by daniela on 12.10.2016.
 */
public class Main extends JavaPlugin {

    public FileConfiguration cfg;
    public Map<String, Command> commands;
    public String prefix;
    public CommandParser commandParser;
    public CommandExecutor commandExecutor;


    @Override
    public void onEnable() {
        this.loadPlugin();
    }

    @Override
    public void onDisable() {
        this.unloadPlugin();
    }

    public void restartPlugin() {
        this.unloadPlugin();
        this.reloadConfig();
        this.loadPlugin();
    }

    public void loadPlugin() {

        this.cfg = this.getConfig();
        this.commands = new HashMap<String, Command>();
        this.prefix = ChatColor.GRAY + "[" + ChatColor.DARK_RED + "AAE" + ChatColor.GRAY + "] " + ChatColor.AQUA;
        this.commandParser = new CommandParser(this);
        this.commandExecutor = new CommandExecutor(this);

        // Register /alienautoexecutor
        new AlienAutoExecutorCommandExecutor(this, "alienautoexecutor", "Executes a bunch of commands automatically upon command", "aae");

        if(!this.cfg.contains("enabled-commands")) {
            this.error("tooFewCommandsSpecified");
            return;
        }

        ArrayList<String> mainCommands = new ArrayList<String>(this.cfg.getConfigurationSection("enabled-commands").getKeys(false));
        if(mainCommands.size() == 2) {
            if(mainCommands.get(0).equals("command1") &&
               mainCommands.get(1).equals("command2")) {
                this.error("tooFewCommandsSpecified");
                return;
            }
        }


        for(String cmdname : mainCommands) {

            String cmdnameSmall = cmdname.toLowerCase();

            ArrayList<String> section = new ArrayList<String>(this.cfg.getStringList("enabled-commands." + cmdname));

            String helpMessage = this.commandParser.getHelpMessage(section, cmdnameSmall);
            ArrayList<String> subcommands = this.commandParser.getSubcommands(section);
            ArrayList<String> aliases = this.commandParser.getAliases(section);

            Command command = new Command(cmdnameSmall, helpMessage, subcommands, aliases);
            this.commands.put(command.getName(), command);

            new AutoCommandExecutor(this, command.getName(), command.getHelpMessage(), command.getAliases());

        }

    }

    private void unloadPlugin() {

        if(!this.commands.isEmpty()) {
            for(Map.Entry<String, Command> command : this.commands.entrySet()) {
                CommandUnregisterer.unRegisterBukkitCommand(command.getValue());
            }
        }

    }

    public void error(String error, String... args) {
        if(error.equals("tooFewCommandsSpecified")) {
            this.cfg.set("enabled-commands", new HashMap<String, ArrayList<String>>(){{
                put("command1", new ArrayList<String>() {{ add("help:&cExecute a bunch of commands"); add("alias:cmd1"); add("/help"); add("/?"); add("wait:50"); add("/bann $1"); add("/pexx user $1 add $2"); }});
                put("command2", new ArrayList<String>() {{ add("help:&cExecute another bunch of commands"); add("alias:cmd2"); add("/difficulty $1"); add("wait:100"); add("/difficulty $2"); }});
            }});
            this.saveConfig();

            this.getLogger().severe("=========================================================================");
            this.getLogger().severe("== You need to specify commands in plugins/AlienAutoExecutor/config.yml ==");
            this.getLogger().severe("=========================================================================");
            return;
        }

        if(error.equals("noPluginDescription")) {
            String s = "";
            for(int i = 0; i < args[0].length(); i++) {s+="=";}
            this.getLogger().severe("==================================================================================================================" + s);
            this.getLogger().severe("== The help message for " + args[0] + " does not exist (add \"- help:Custom message\" in plugins/AlienAutoExecutor/config.yml) ==");
            this.getLogger().severe("==================================================================================================================" + s);
            return;
        }

        if(error.equals("noInteger")) {
            String s = "";
            for(int i = 0; i < args[0].length(); i++) {s+="=";}
            this.getLogger().severe("==================================" + s);
            this.getLogger().severe("== Integer required in config: " + args[0] + " ==");
            this.getLogger().severe("==================================" + s);
            return;
        }
    }

}
