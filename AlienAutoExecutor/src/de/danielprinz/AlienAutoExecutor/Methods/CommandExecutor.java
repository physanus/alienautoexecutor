package de.danielprinz.AlienAutoExecutor.Methods;

import de.danielprinz.AlienAutoExecutor.Main;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by daniela on 12.10.2016.
 */
public class CommandExecutor {

    private Main plugin;
    public CommandExecutor(Main plugin) {
            this.plugin = plugin;
    }


    public void load(final ArrayList<String> subcommands, final CommandSender cs, final String[] args) {
        Bukkit.getScheduler().runTaskAsynchronously(this.plugin, new Runnable() {
            @Override
            public void run() {

                for(String cmd : subcommands) {
                    getPlugin().getLogger().info(cs.getName() + " is executing " + cmd + " with args " + Arrays.asList(args));

                    if(cmd.startsWith("wait:")) {
                        int time = Integer.parseInt(cmd.replaceAll("wait:", ""));
                        try {
                            Thread.sleep(time * 1000 / 20);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        continue;
                    }

                    String commandTmp = cmd;

                    // remove leading slash
                    commandTmp = commandTmp.replaceFirst("/", "");

                    // transform $n to args[n]
                    while(commandTmp.contains("$")) {
                        int index = commandTmp.indexOf("$");
                        if(!(index == -1)) {
                            String arg1 = String.valueOf(commandTmp.charAt(index)) + String.valueOf(commandTmp.charAt(index + 1));
                            String arg2 = args[Integer.parseInt(String.valueOf(commandTmp.charAt(index + 1))) - 1];
                            commandTmp = commandTmp.replaceAll(String.valueOf('\\') + arg1, arg2);
                        }
                    }

                    final String finalCommandTmp = commandTmp;
                    Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
                        @Override
                        public void run() {
                            Bukkit.getServer().dispatchCommand(cs, finalCommandTmp);
                        }
                    });

                }
            }
        });
    }

    private Main getPlugin() {
        return this.plugin;
    }
}
