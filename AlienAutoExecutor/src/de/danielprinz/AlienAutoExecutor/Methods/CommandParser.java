package de.danielprinz.AlienAutoExecutor.Methods;

import de.danielprinz.AlienAutoExecutor.Main;
import org.bukkit.ChatColor;

import java.util.ArrayList;

/**
 * Created by daniela on 12.10.2016.
 */
public class CommandParser {

    private Main plugin;
    public CommandParser(Main plugin) {
        this.plugin = plugin;
    }


    public String getHelpMessage(ArrayList<String> section, String cmdname) {
        for (String sec : section) {
            if (sec.startsWith("help:")) {
                sec = sec.replaceAll("help:", "");
                sec = ChatColor.translateAlternateColorCodes('&', sec);
                return sec;
            }
        }

        this.plugin.error("noPluginDescription", cmdname);
        return null;
    }

    public ArrayList<String> getSubcommands(ArrayList<String> section) {
        ArrayList<String> subcommands = new ArrayList<String>();

        for(String sec : section) {
            if(sec.startsWith("/")) {

                String secTmp = sec;
                while(secTmp.contains("$")) {
                    try {
                        Integer.parseInt(String.valueOf(secTmp.charAt(secTmp.indexOf("$") + 1)));
                        secTmp = secTmp.substring(secTmp.indexOf("$") + 1);
                    } catch (NumberFormatException e) {
                        this.plugin.error("noInteger", sec);
                    }
                }
                subcommands.add(sec);
            }
            if(sec.startsWith("wait:")) {
                try {
                    Integer.parseInt(sec.replaceAll("wait:", ""));
                    subcommands.add(sec);
                } catch (NumberFormatException e) {
                    this.plugin.error("noInteger", sec);
                }
            }
        }

        return subcommands;
    }

    public ArrayList<String> getAliases(ArrayList<String> section) {
        ArrayList<String> aliases = new ArrayList<String>();

        for(String sec : section) {
            if(sec.startsWith("alias:")) {
                aliases.add(sec.replaceAll("alias:", ""));
            }
        }
        return aliases;
    }

}
