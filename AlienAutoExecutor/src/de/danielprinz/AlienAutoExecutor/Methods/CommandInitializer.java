package de.danielprinz.AlienAutoExecutor.Methods;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandMap;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

/**
 * Created by daniela on 12.10.2016.
 */
public abstract class CommandInitializer<Plugin extends JavaPlugin> extends Command {

    private static String VERSION;

    static {
        String path = Bukkit.getServer().getClass().getPackage().getName();
        CommandInitializer.VERSION = path.substring(path.lastIndexOf(".") + 1, path.length());
    }

    protected final Plugin plugin;
    protected final String command;

    public CommandInitializer(Plugin plugin, String command, String description, String... aliases) {
        super(command);
        this.plugin = plugin;
        this.command = command;

        super.setDescription(description);

        List<String> aliasList = Arrays.asList(aliases);
        super.setAliases(aliasList);

        this.register();
    }

    public void register() {
        try {
            Field f = Class.forName("org.bukkit.craftbukkit." + CommandInitializer.VERSION + ".CraftServer").getDeclaredField("commandMap");
            f.setAccessible(true);

            CommandMap map = (CommandMap) f.get(Bukkit.getServer());
            map.register(this.plugin.getName(), this);

        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }

    public abstract boolean execute(CommandSender cs, String label, String[] args);

    public abstract List<String> tabComplete(CommandSender cs, String label, String[] args);

    public String buildString(String[] args, int start) {
        String str = "";
        if (args.length > start) {
            str += args[start];
            for (int i = start + 1; i < args.length; i++) {
                str += " " + args[i];
            }
        }
        return str;
    }

    public Plugin getPlugin() {
        return this.plugin;
    }
}
